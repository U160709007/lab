package shapes;

public class Circle extends Shape{
	private double side;

	
	public Circle(double side) {
		super();
		this.side = side;
	}

	public double area(){
		return side*side;
	}
	
	
}
