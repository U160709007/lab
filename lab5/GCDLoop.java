public class GCDLoop{


    public static void main(String[] args){
       
       int a =Integer.parseInt(args[0]);
       int b=Integer.parseInt(args[1]);

       int result=gcd(a,b);
       System.out.println(result);
    }
    

    public static int gcd(int a, int b){
        int r=0;
    
        if(a>b){
            
            do{
                r= a%b;
                if(r==0){
                    return b;
                    
                }
                else{
                    a=b;
                    b=r;
                    
                }
       
            } while(r!=0); 
    
        }
        else if(a<b){
            
            do{
                r= b%a;
                if(r==0){
                    return a;
                    
                }
                else{
                    b=a;
                    a=r;
                    continue;
                }

            }while(r!=0);
        }
        
        return a;
        
        
    }   
        
}