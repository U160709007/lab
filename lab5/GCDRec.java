
public class GCDRec {

    public static void main(String[] args) {
        int a =Integer.parseInt(args[0]);
        int b=Integer.parseInt(args[1]);

        int result=rec(a,b);
        System.out.println(result);
    }

    public static int rec(int a, int b) {
        int r=b;
        if (a%b!=0) {
            b=a%b;
            b=rec(r, b);
        }
        return b;
    }

}