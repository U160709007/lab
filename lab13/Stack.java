package lab;

public interface Stack {
	void push(Object obj);
	Object pop();
	boolean empty();

}