package lab;

public class StackImpl implements Stack {
	
	private Stackitem top; 
	
	
	public void push(Object obj) {
		 Stackitem item = new Stackitem(obj);
		 item.setNext(top);
		 top = item;
	 }
	public Object pop() {
		 Stackitem item = top;
		 top = top.getNext();
		 return item.getObj();
	}
	public boolean empty() {
		return top == null;
	}
}
