import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		int row,col;
		boolean win;
		printBoard(board);
		do {
			do {
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				col = reader.nextInt();
			} while ((row>3 || col>3) || (board[row - 1][col - 1]!=' ') );
			board[row - 1][col - 1] = 'X';
			printBoard(board);
			if (checkboard(board,'X')) {
				System.out.println("Player 1 is WINNER");
				break;
			}
			else if (!isEmpty(board)) {
				System.out.println("That games ended with a draw");
				break;
			}
			do {
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();
			} while ((row>3 || col>3) || (board[row - 1][col - 1]!=' ') );
			board[row - 1][col - 1] = 'O';
			printBoard(board);
			if (checkboard(board,'O')) {
				System.out.println("Player 2 is WINNER");
				break;
			}
		} while (isEmpty(board));
		

		reader.close();
	}

	public static boolean isEmpty(char[][] array) {
		boolean isNull = false;
		for(char[] arrayX : array){
			for(char val : arrayX){
				if(val==' '){
					isNull=true;
					break;
				}
			}
		}
		return isNull;
	}
	
	public static boolean checkboard(char[][] array, char symbol) {
		boolean winner = false;
		if (array[0][0]==symbol&&array[0][1]==symbol&&array[0][2]==symbol) {
			winner=true;
		}
		else if (array[1][0]==symbol&&array[1][1]==symbol&&array[1][2]==symbol) {
			winner=true;
		}
		else if (array[2][0]==symbol&&array[2][1]==symbol&&array[2][2]==symbol) {
			winner=true;
		}
		else if (array[0][0]==symbol&&array[1][0]==symbol&&array[2][0]==symbol) {
			winner=true;
		}
		else if (array[0][1]==symbol&&array[1][1]==symbol&&array[2][1]==symbol) {
			winner=true;
		}
		else if (array[0][2]==symbol&&array[1][2]==symbol&&array[2][2]==symbol) {
			winner=true;
		}
		else if (array[0][0]==symbol&&array[1][1]==symbol&&array[2][2]==symbol) {
			winner=true;
		}
		else if (array[2][0]==symbol&&array[1][1]==symbol&&array[2][0]==symbol) {
			winner=true;
		}
		return winner;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}

