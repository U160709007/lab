
import java.lang.Object;


public class Square{

    int side;

    public Square(int side){
        this.side=side;
    }
    

    public int  area(){
        return side*side;
    }
    public String toString(){
        return "side = " + side;

    }
    



}