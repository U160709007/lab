//If you write your own constructor, the default constructor will diseappear.
//Instance variable:If the variable is inside the class and outside the functions and they are available when there is a object and their default value is 0.
//null is just available for objects.
//Local variables use inside the methods or in loops. They have a scope.
//We can access the static variable from class name like Point.variablename
//Static variable is same for the all objects.


public class Rectangle{
	
	int sideA;
	int sideB;
	
	Point topLeft;
	
	public Rectangle(int x, int y, Point p){
		sideA=x;
		sideB=y;
		topLeft=p;
		
	}
	
	public int area() {
		return sideA*sideB;
	}
	
	public int perimeter() {
		
		return((sideA+sideB)*2);
		
	}
	public Point[] corners(){
		Point[] corners = new Point[4];
		corners[0] = topLeft;
	    corners[1] = new Point(topLeft.xCoord + sideA, topLeft.yCoord );
		corners[2] = new Point(topLeft.xCoord,+  topLeft.yCoord + sideB);
		corners[3] = new Point(topLeft.xCoord + sideA, topLeft.yCoord + sideB);
		return corners;
		
		
		
		
		
	
		
	}
}
