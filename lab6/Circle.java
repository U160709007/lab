public class Circle{
	int radius;
	Point center;
	
	public Circle(int a, Point b ) {
		radius=a;
		center=b;
	}
	
	public double area() {
		return(Math.PI*radius*radius);
	}
	
	public double perimeter() {
		return(2*Math.PI*radius);
	}
	
	public boolean intersect(Circle c){
	    double distance = Math.sqrt(Math.pow(center.xCoord - c.center.xCoord, 2 ) + Math.pow(center.yCoord - c.center.yCoord, 2));
		return distance < c.radius + radius;
	}
	
}

